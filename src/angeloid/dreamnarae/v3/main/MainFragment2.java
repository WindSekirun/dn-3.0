package angeloid.dreamnarae.v3.main;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.tweak.DPCheck;

public class MainFragment2 extends Fragment {
	RelativeLayout rl_dp;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main2, container, false);
		rl_dp = (RelativeLayout) v.findViewById(R.id.rl_dp);
		rl_dp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getActivity(), DPCheck.class);
				startActivity(i);
				
			}
		});
		return v;
	}

}
