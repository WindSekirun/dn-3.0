package angeloid.dreamnarae.v3.main;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.tweak.Tweak;

public class MainFragment1 extends Fragment {
	
	RelativeLayout rl_tweak;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main1, container, false);
		rl_tweak = (RelativeLayout) v.findViewById(R.id.rl_tweak);
		rl_tweak.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getActivity(), Tweak.class);
				startActivity(i);
				
			}
		});
		return v;
	}

}