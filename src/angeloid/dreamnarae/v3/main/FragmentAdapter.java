package angeloid.dreamnarae.v3.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FragmentAdapter extends FragmentPagerAdapter{
	
	private int mCount = 3;

	public FragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = new MainFragment1();
		switch(position){
		case 0:
			fragment = new MainFragment1();
			break;
		case 1:
			fragment = new MainFragment2();
			break;
		case 2:
			fragment = new MainFragment3();
			break;
		}
		return fragment;
	}

	@Override
	public int getCount() {
		return mCount;
	}
	
	public void setCount(int count){
		if (count > 0 && count < 10){
			mCount = count;
			notifyDataSetChanged();
		}
	}
	

}
