package angeloid.dreamnarae.v3.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.Toast;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import angeloid.Security.IAB.IabHelper;
import angeloid.Security.IAB.IabResult;
import angeloid.dreamnarae.v3.PreferenceActivity;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.service.EntropyService;
import angeloid.dreamnarae.v3.service.RestartService;
import angeloid.dreamnarae.v3.tutorial.TutorialActivity;

import com.android.vending.billing.IInAppBillingService;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;
import com.viewpagerindicator.LinePageIndicator;

public class MainActivity extends Activity {
	FragmentAdapter mAdapter;
	ViewPager mPager;
	LinePageIndicator mIndicator;
	long backPressedTime = 0;
	TextView tweakuse;
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	String tweakName;
	boolean entropy;
	ImageView left;
	ImageView right;
	boolean arrow;
	static IInAppBillingService mService;
	static IabHelper mHelper;
	static Context c;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), mServiceConn,
				Context.BIND_AUTO_CREATE);
		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainpager);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xffffffff));
		getSupportActionBar().setTitle(R.string.main);
		getSupportActionBar().setIcon(R.drawable.icon);
		left = (ImageView) findViewById(R.id.left);
		right = (ImageView) findViewById(R.id.right);
		pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
		String tutorial = pref.getString("tutorial", "false");
		if (tutorial.equals("false")) {
			startActivity(new Intent(MainActivity.this, TutorialActivity.class));
		}
		arrow = pref.getBoolean("arrow", true);
		if (arrow == false) {
			left.setVisibility(4);
			right.setVisibility(4);
		} else {
			left.setVisibility(0);
			right.setVisibility(0);
		}
		mAdapter = new FragmentAdapter(getSupportFragmentManager());

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		mIndicator = (LinePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		mIndicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				int id = mPager.getCurrentItem();
				if (arrow == true) {
					if (id == 0) {
						left.setVisibility(4);
					} else if (id == 2) {
						right.setVisibility(4);
					} else if (id == 1) {
						left.setVisibility(0);
						right.setVisibility(0);
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		entropy = pref.getBoolean("entropy", true);

		if (isDetected(MainActivity.this, "com.lcis.seeder")) {
			Seeder_Detected();
		}

		if (isDetected(MainActivity.this, "angeloid.dreamnarae")) {
			OldDN_Detected();
		}

		if (entropy == true) {
			unregisterRestartAlarm();
			Intent intent = new Intent(MainActivity.this, EntropyService.class);
			startService(intent);
		}

		editor = pref.edit();
		tweakuse = (TextView) findViewById(R.id.tweakName);
		tweakName = pref.getString("tweak", null);

		if (tweakName != null) {
			if (tweakName.equals("Angel")) {
				tweakuse.setText(getString(R.string.useangel));
			} else if (tweakName.equals("Fable")) {
				tweakuse.setText(getString(R.string.usefable));
			} else if (tweakName.equals("Polar")) {
				tweakuse.setText(getString(R.string.usepolar));
			}
		} else {
			tweakuse.setText(getString(R.string.nottweak));
		}

	}
	
	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}
	};

	public static boolean isPro() {
		try {
			Bundle ownedItems = mService.getPurchases(3, "angeloid.dreamnarae.v3", "inapp", null);
			int response = ownedItems.getInt("RESPONSE_CODE");
			ArrayList<String> ownedSkus = null;

			if (response == 0) {
				ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
			}

			if (response == 0 && ownedSkus.size() > 0 && ownedSkus.get(0).equals("angel_unlock"))
				return true;
		} catch (RemoteException e) {
		}

		return false;

	}

	public void Seeder_Detected() {
		AlertDialog.Builder narae = new AlertDialog.Builder(MainActivity.this);
		narae.setTitle(R.string.seeder_discovered_);
		narae.setMessage(R.string.seedermessage);
		narae.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent i = new Intent(Intent.ACTION_DELETE);
						i.setData(Uri.parse("package:com.lcis.seeder"));
						startActivity(i);
					}
				});
		narae.show();
	}

	public void OldDN_Detected() {
		AlertDialog.Builder narae = new AlertDialog.Builder(MainActivity.this);
		narae.setTitle(R.string.old_dreamnarae_discovered_);
		narae.setMessage(R.string.dnmessage);
		narae.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent i = new Intent(Intent.ACTION_DELETE);
						i.setData(Uri.parse("package:angeloid.dreamnarae"));
						startActivity(i);
						RootTools.remount("/system/", "RW");
						CommandCapture command = new CommandCapture(0,
								"rm /system/98banner_dreamnarae_spica",
								"rm /system/98banner_dreamnarae_miracle",
								"rm /system/98banner_dreamnarae_save",
								"rm /system/98banner_dreamnarae_prev",
								"rm /system/98banner_dreamnarae_pure",
								"rm /system/98banner_dreamnarae_brand",
								"rm /system/98banner_dreamnarae_spisave",
								"rm /system/etc/dreamnarae.sh");
						try {
							RootTools.getShell(true).add(command)
									.waitForFinish();
						} catch (InterruptedException e) {
						} catch (IOException e) {
						} catch (TimeoutException e) {
						} catch (RootDeniedException e) {
						}
					}
				});
		narae.show();
	}

	@Override
	public void onResume() {
		super.onResume();
		tweakName = pref.getString("tweak", null);

		if (tweakName != null) {
			if (tweakName.equals("Angel")) {
				tweakuse.setText(getString(R.string.useangel));
			} else if (tweakName.equals("Fable")) {
				tweakuse.setText(getString(R.string.usefable));
			} else if (tweakName.equals("Polar")) {
				tweakuse.setText(getString(R.string.usepolar));
			}
		} else {
			tweakuse.setText(getString(R.string.nottweak));
		}
		arrow = pref.getBoolean("arrow", true);
		if (arrow == false) {
			ImageView left = (ImageView) findViewById(R.id.left);
			ImageView right = (ImageView) findViewById(R.id.right);
			left.setVisibility(4);
			right.setVisibility(4);
		}
		
		mIndicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				int id = mPager.getCurrentItem();
				if (arrow == true) {
					if (id == 0) {
						left.setVisibility(4);
					} else if (id == 2) {
						right.setVisibility(4);
					} else if (id == 1) {
						left.setVisibility(0);
						right.setVisibility(0);
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
	}

	@Override
	public void onBackPressed() {
		long tempTime = System.currentTimeMillis();
		long intervalTime = tempTime - backPressedTime;

		if (0 <= intervalTime && 2000 >= intervalTime)
			super.onBackPressed();
		else {
			backPressedTime = tempTime;
			Toast.makeText(MainActivity.this, R.string._1_more_click_to_exit,
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isDetected(Context c, String strPackageName) {
		PackageInfo pi = null;
		try {
			pi = c.getPackageManager().getPackageInfo(strPackageName, 0);
		} catch (NameNotFoundException e) {

		}
		if (pi != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_setting) {
			startActivity(new Intent(MainActivity.this,
					PreferenceActivity.class));
		}
		return super.onOptionsItemSelected(item);
	}

	void registerRestartAlarm() {
		Intent intent = new Intent(MainActivity.this, RestartService.class);
		intent.setAction("ACTION_RESTART_PERSISTENTSERVICE");
		PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, 0,
				intent, 0);
		long firstTime = SystemClock.elapsedRealtime();
		firstTime += 1 * 1000;
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
				2 * 1000, sender);
	}

	void unregisterRestartAlarm() {
		Intent intent = new Intent(MainActivity.this, RestartService.class);
		intent.setAction("ACTION_RESTART_PERSISTENTSERVICE");
		PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, 0,
				intent, 0);
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.cancel(sender);
	}

	@Override
	public void onDestroy() {
		if (entropy == true) {
			registerRestartAlarm();
		}
		if (mServiceConn != null) {
			unbindService(mServiceConn);
		}
		super.onDestroy();
	}
}