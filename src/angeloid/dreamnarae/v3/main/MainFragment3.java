package angeloid.dreamnarae.v3.main;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.guide.GuideActivity;

public class MainFragment3 extends Fragment {
	
	RelativeLayout rl_guide;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main3, container, false);
		rl_guide = (RelativeLayout) v.findViewById(R.id.rl_guide);
		rl_guide.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getActivity(), GuideActivity.class);
				startActivity(i);
				
			}
		});
		return v;
	}

}