package angeloid.dreamnarae.v3;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.drawable.ColorDrawable;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import angeloid.dreamnarae.v3.main.MainActivity;

public class NotRoot extends Activity {
	String strLanguage;
	Locale systemLocale;
	Button problem;
	String assetTxt;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		setContentView(R.layout.noroot);
		getSupportActionBar().setTitle(R.string.error);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xffffffff));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setIcon(R.drawable.icon);
		systemLocale = getResources().getConfiguration().locale;
		strLanguage = systemLocale.getLanguage();
		problem = (Button) findViewById(R.id.button1);
		problem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Dialog dialog1 = new Dialog(NotRoot.this);
				dialog1.setContentView(R.layout.license);
				dialog1.setTitle(R.string.narae_);
				dialog1.setCancelable(true);
				TextView text = (TextView) dialog1
						.findViewById(R.id.updatetext);
				try {
					if (strLanguage.equals("ko")) {
						assetTxt = readText("root/rootproblem(ko).txt");
					} else {
						assetTxt = readText("root/rootproblem(eng).txt");
					}
					text.setText(assetTxt);
					dialog1.show();
				} catch (IOException e) {
				}

			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			startActivity(new Intent(this, MainActivity.class));
		}
		return super.onOptionsItemSelected(item);
	}

	private String readText(String file) throws IOException {
		InputStream is = this.getAssets().open(file);

		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();

		String text = new String(buffer);

		return text;
	}
}
