package angeloid.dreamnarae.v3.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.tweak.GetTweak;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;

public class BootReceiver extends BroadcastReceiver {

	SharedPreferences prefs;
	boolean notification;
	boolean entropy;
	String entropy_term;
	CharSequence boot_ticker;
	CharSequence boot_bottom;
	CharSequence boot_top;
	NotificationManager manager;
	NotificationCompat.Builder ncbuilder;
	ArrayList<String> property;
	int check;
	String naraecheck;
	String output;
	CommandCapture tweak;
	String tweakName;

	@Override
	public void onReceive(Context c, Intent intent) {
		prefs = PreferenceManager.getDefaultSharedPreferences(c);
		notification = prefs.getBoolean("notification", true);
		entropy = prefs.getBoolean("entropy", true);
		boot_top = c.getString(R.string.app_name);
		boot_bottom = c.getString(R.string.boot_bottom);
		boot_ticker = c.getString(R.string.app_name);
		manager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		ncbuilder = new NotificationCompat.Builder(c);
		ncbuilder.setContentTitle(boot_top);
		ncbuilder.setContentText(boot_bottom);
		ncbuilder.setSmallIcon(R.drawable.ic_launcher);
		ncbuilder.setAutoCancel(true);
		ncbuilder.setTicker(boot_ticker);
		tweakName = prefs.getString("tweak", null);
		naraecheck = prefs.getString("narae", "notuse");

		if (tweakName != null) {
			if (tweakName.equals("Angel")) {
				check = Integer.valueOf(check) + 1;
				if (naraecheck.equals("use")) {
					output = new String(GetTweak.Angel_DP());
				} else {
					output = new String(GetTweak.Angel_DP_Setprop());
				}
			} else if (tweakName.equals("Fable")) {
				check = Integer.valueOf(check) + 1;
				if (naraecheck.equals("use")) {
					output = new String(GetTweak.Fable_DP());
				} else {
					output = new String(GetTweak.Fable_DP_Setprop());
				}
			} else if (tweakName.equals("Polar")) {
				check = Integer.valueOf(check) + 1;
				if (naraecheck.equals("use")) {
					output = new String(GetTweak.Polar_DP());
				} else {
					output = new String(GetTweak.Polar_DP_Setprop());
				}
			}
		}

		if (RootTools.isAccessGiven() == true) {
			if (entropy == true) {
				intent = new Intent(c, EntropyService.class);
				c.startService(intent);
			}

			if (check > 0) {

				if (notification == true) {
					manager.notify(0, ncbuilder.build());
				}

				CommandCapture entropy = new CommandCapture(0,
						"/system/xbin/rngd -t 2 -T 1 -s 256 --fill-watermark=80%",
						"/system/bin/rngd -t 2 -T 1 -s 256 --fill-watermark=80%", "sleep 2",
						"echo -8 > /proc/$(pgrep rngd)/oom_adj", "renice 5 `pidof rngd`");

				CommandCapture command = new CommandCapture(0, "mount -o rw,remount /system",
						"busybox run-parts /system/etc/init.d", "sh /system/etc/install-recovery.sh");

				Scanner s = new Scanner(output);
				property = new ArrayList<String>();
				while ((s.hasNext())) {
					property.add(s.nextLine());
				}
				s.close();
				FileOutputStream fos = null;
				String str = null;

				try {
					fos = c.openFileOutput(c.getCacheDir() + "main.txt", Context.MODE_PRIVATE);
					str = output;
					fos.write(str.getBytes());
					fos.close();
					RootTools.remount("/system/", "rw");
					RootTools.runBinary(c, "mkdir", "/system/etc/dreamnarae");
					RootTools.copyFile(c.getCacheDir() + "/main.txt", "/system/etc/dreamnarae/main.sh", true, false);
					RootTools.runBinary(c, "sh", "/system/etc/dreamnarae/main.sh");
				} catch (FileNotFoundException e1) {
				} catch (IOException e) {
				}
				manager.notify(0, ncbuilder.build());
				try {
					RootTools.getShell(true).add(command).waitForFinish();
					RootTools.getShell(true).add(entropy).waitForFinish();
				} catch (InterruptedException e) {
				} catch (IOException e) {
				} catch (TimeoutException e) {
				} catch (RootDeniedException e) {
				}
			}
		} else {
			boot_bottom = c.getString(R.string.boot_fail);
			ncbuilder.setContentText(boot_bottom);
			manager.notify(0, ncbuilder.build());
		}
	}
}