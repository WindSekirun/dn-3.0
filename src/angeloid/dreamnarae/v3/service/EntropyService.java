package angeloid.dreamnarae.v3.service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;

public class EntropyService extends Service {

	SharedPreferences prefs;
	String term;
	long milesecond;
	boolean service_notification;
	String level;
	int level_amount;

	@Override
	public IBinder onBind(Intent i) {
		return null;
	}

	@Override
	public void onCreate() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		term = prefs.getString("entropyterm", "10");
		level = prefs.getString("entropylevel", "0");
		if (term != null) {
			if (term.equals("1")) {
				milesecond = (long) 1 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("5")) {
				milesecond = (long) 5 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("10")) {
				milesecond = (long) 10 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("20")) {
				milesecond = (long) 20 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("40")) {
				milesecond = (long) 40 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("1h")) {
				milesecond = (long) 60 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("3h")) {
				milesecond = (long) 180 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("6h")) {
				milesecond = (long) 360 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			} else if (term.equals("10h")) {
				milesecond = (long) 600 * 60 * 1000;
				Log.e("EntropyTerm", String.valueOf(milesecond));
			}
		}
		if (level.equals("0")) {
			level_amount = 80;
		} else if (level.equals("+3")) {
			level_amount = 95;
		} else if (level.equals("+2")) {
			level_amount = 90;
		} else if (level.equals("+1")) {
			level_amount = 85;
		} else if (level.equals("-1")) {
			level_amount = 75;
		} else if (level.equals("-2")) {
			level_amount = 70;
		} else if (level.equals("-3")) {
			level_amount = 65;
		}
		unregisterRestartAlarm();
		super.onCreate();
		Handler mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				CommandCapture entropy = new CommandCapture(0,
						"/system/xbin/rngd -t 2 -T 1 -s 256 --fill-watermark="
								+ level_amount + "%",
						"/system/bin/rngd -t 2 -T 1 -s 256 --fill-watermark="
								+ level_amount + "%", "sleep 2",
						"echo -8 > /proc/$(pgrep rngd)/oom_adj",
						"renice 5 `pidof rngd`");
				try {
					RootTools.getShell(true).add(entropy).waitForFinish();
				} catch (InterruptedException e) {
				} catch (IOException e) {
				} catch (TimeoutException e) {
				} catch (RootDeniedException e) {
				}
			}

		}, milesecond);
	}

	@Override
	public void onDestroy() {
		registerRestartAlarm();
		super.onDestroy();
	}

	void registerRestartAlarm() {
		Intent intent = new Intent(EntropyService.this, RestartService.class);
		intent.setAction("ACTION_RESTART_PERSISTENTSERVICE");
		PendingIntent sender = PendingIntent.getBroadcast(EntropyService.this,
				0, intent, 0);
		long firstTime = SystemClock.elapsedRealtime();
		firstTime += 1 * 1000;
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime,
				2 * 1000, sender);
	}

	void unregisterRestartAlarm() {
		Intent intent = new Intent(EntropyService.this, RestartService.class);
		intent.setAction("ACTION_RESTART_PERSISTENTSERVICE");
		PendingIntent sender = PendingIntent.getBroadcast(EntropyService.this,
				0, intent, 0);
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.cancel(sender);
	}

}