package angeloid.dreamnarae.v3.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RestartService extends BroadcastReceiver {

	@Override
	public void onReceive(Context c, Intent i) {
		Intent intent = new Intent(c, EntropyService.class);
		c.startService(intent);
	}
}
