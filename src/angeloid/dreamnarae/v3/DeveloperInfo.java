package angeloid.dreamnarae.v3;

import org.holoeverywhere.app.Activity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DeveloperInfo extends Activity {

	ListView list;
	
	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		setContentView(R.layout.guide4);
		list = (ListView) findViewById(R.id.list);
		ArrayAdapter<CharSequence> developerinfo = ArrayAdapter
				.createFromResource(DeveloperInfo.this, R.array.developer,
						R.layout.listview);
		list.setAdapter(developerinfo);
	}
}