package angeloid.dreamnarae.v3.tweak;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.download.DownloadTask;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;

public class Narae extends AsyncTask<String, String, Void> {
	
	private ProgressDialog mDlg;
	private static Context C;
	ArrayList<String> property;
	CommandCapture tweak;
	String output;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	static String url;
	
	public Narae(Context context) {
		C = context;
	}
	
	public static void Download() {
		url = "http://gecp.kr/dn/dnbinary.zip";
		new DownloadTask(C).execute(url);
	}

	@Override
	protected Void doInBackground(String... params) {
		final String TweakName = params[0];
		prefs = PreferenceManager.getDefaultSharedPreferences(C);
		editor = prefs.edit();
		for (int i = 0; i < 6; i++) {
			if (i == 0) {
				try {
					publishProgress("p0");
					Thread.sleep(5000);
					
					Download();
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
			} else if (i == 1) {
				publishProgress("p1");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
				}
				editor = prefs.edit();
				editor.putString("narae", "use");
				editor.commit();
				RootTools.remount("/system/", "rw");
				RootTools.copyFile(C.getExternalFilesDir(null)
						+ "/rngd", "/system/bin/rngd", true, false);
				RootTools.copyFile(C.getExternalFilesDir(null)
						+ "/zipAlign", "/system/bin/zipAlign", true, false);
				RootTools.copyFile(C.getExternalFilesDir(null)
						+ "/rngd", "/system/xbin/rngd", true, false);
				RootTools.copyFile(C.getExternalFilesDir(null)
						+ "/narae", "/system/xbin/narae", true, false);
				RootTools.copyFile(C.getExternalFilesDir(null)
						+ "/narae", "/system/bin/narae", true, false);
				RootTools
						.copyFile(C.getExternalFilesDir(null)
								+ "/zipAlign", "/system/xbin/zipAlign",
								true, false);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
			} else if (i == 2) {
				publishProgress("p2");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
				}
				RootTools.remount("/system/", "rw");
				CommandCapture command = new CommandCapture(0,
						"chmod 777 /system/bin/rngd",
						"chmod 777 /system/xbin/rngd",
						"chmod 777 /system/bin/zipAlign",
						"chmod 777 /system/xbin/zipAlign",
						"chmod 777 /system/bin/narae",
						"chmod 777 /system/xbin/narae");
				try {
					RootTools.getShell(true).add(command).waitForFinish();
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				} catch (IOException e) {
				} catch (TimeoutException e) {
				} catch (RootDeniedException e) {
				}
			} else if (i == 3) {
				publishProgress("p3");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
				}
				if (TweakName != null) {
					if (TweakName.equals("Angel")) {
						output = new String(GetTweak.Angel_DP());
					} else if (TweakName.equals("Fable")) {
						output = new String(GetTweak.Fable_DP());
					} else if (TweakName.equals("Polar")) {
						output = new String(GetTweak.Polar_DP());
					}
				}
				Scanner s = new Scanner(output);
				property = new ArrayList<String>();
				while ((s.hasNext())) {
					property.add(s.nextLine());
				}
				s.close();

				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
			} else if (i == 4) {
				publishProgress("p4");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
				}
			} else if (i == 5) {
				publishProgress("p5");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
				}
				editor.putString("tweak", TweakName);
				editor.commit();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		publishProgress("dismiss");
		AlertDialog.Builder naraeAlert = new AlertDialog.Builder(C);
		naraeAlert.setMessage(C.getString(R.string.reboot_));
		naraeAlert.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						CommandCapture command = new CommandCapture(0,
								"reboot");
						try {
							RootTools.getShell(true).add(command)
									.waitForFinish();
						} catch (InterruptedException e) {
						} catch (IOException e) {
						} catch (TimeoutException e) {
						} catch (RootDeniedException e) {
						}
					}
				});
		naraeAlert.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
					}
				});

		naraeAlert.show();
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() {
		mDlg = new ProgressDialog(C);
		mDlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mDlg.setMessage(C.getString(R.string.installing));
		mDlg.setIndeterminate(false);
		mDlg.setCancelable(false);
		mDlg.setCanceledOnTouchOutside(false);
		mDlg.setMax(100);
		mDlg.show();

		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(String... progress) {
		if (progress[0].equals("p0")) {
			mDlg.setProgress(10);
			mDlg.setMessage(C.getString(R.string.p0));
		} else if (progress[0].equals("p1")) {
			mDlg.setProgress(30);
			mDlg.setMessage(C.getString(R.string.p1));
		} else if (progress[0].equals("p2")) {
			mDlg.setProgress(50);
			mDlg.setMessage(C.getString(R.string.p2));
		} else if (progress[0].equals("p3")) {
			mDlg.setProgress(65);
			mDlg.setMessage(C.getString(R.string.p3));
		} else if (progress[0].equals("p4")) {
			mDlg.setProgress(80);
			mDlg.setMessage(C.getString(R.string.p4));
		} else if (progress[0].equals("p5")) {
			mDlg.setProgress(100);
			mDlg.setMessage(C.getString(R.string.p5));
		} else if (progress[0].equals("dismiss")) {
			mDlg.dismiss();
		}
	}
}