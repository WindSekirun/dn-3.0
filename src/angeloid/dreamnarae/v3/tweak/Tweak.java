package angeloid.dreamnarae.v3.tweak;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.drawable.ColorDrawable;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import angeloid.dreamnarae.v3.PreferenceActivity;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.main.MainActivity;

import com.viewpagerindicator.LinePageIndicator;

public class Tweak extends Activity {

	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	String tweakName;
	FragmentAdapter mAdapter;
	ViewPager mPager;
	LinePageIndicator mIndicator;
	TextView tweakuse;
	ImageView left;
	ImageView right;
	boolean arrow;

	@SuppressLint("CommitPrefEdits")
	@Override
	public void onCreate(Bundle S) {
		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		super.onCreate(S);
		setContentView(R.layout.tweakpager);
		left = (ImageView) findViewById(R.id.imageView1);
		right = (ImageView) findViewById(R.id.imageView2);
		prefs = PreferenceManager.getDefaultSharedPreferences(Tweak.this);
		arrow = prefs.getBoolean("arrow", true);
		if (arrow == false) {
			left.setVisibility(4);
			right.setVisibility(4);
		} else {
			left.setVisibility(0);
			right.setVisibility(0);
		}
		editor = prefs.edit();
		// �׼ǹ�
		getSupportActionBar().setTitle(R.string.tweak);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xffffffff));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setIcon(R.drawable.icon);
		mAdapter = new FragmentAdapter(getSupportFragmentManager());

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		mIndicator = (LinePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		mIndicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				int id = mPager.getCurrentItem();
				if (arrow == true) {
					if (id == 0) {
						right.setVisibility(4);
					} else if (id == 2) {
						left.setVisibility(4);
					} else if (id == 1) {
						left.setVisibility(0);
						right.setVisibility(0);
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		tweakuse = (TextView) findViewById(R.id.tweakName);
		prefs = PreferenceManager.getDefaultSharedPreferences(Tweak.this);
		tweakName = prefs.getString("tweak", null);
		if (tweakName != null) {
			if (tweakName.equals("Angel")) {
				tweakuse.setText(getString(R.string.useangel));
			} else if (tweakName.equals("Fable")) {
				tweakuse.setText(getString(R.string.usefable));
			} else if (tweakName.equals("Polar")) {
				tweakuse.setText(getString(R.string.usepolar));
			}
		} else {
			tweakuse.setText(getString(R.string.nottweak));
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		tweakName = prefs.getString("tweak", null);

		if (tweakName != null) {
			if (tweakName.equals("Angel")) {
				tweakuse.setText(getString(R.string.useangel));
			} else if (tweakName.equals("Fable")) {
				tweakuse.setText(getString(R.string.usefable));
			} else if (tweakName.equals("Polar")) {
				tweakuse.setText(getString(R.string.usepolar));
			}
		} else {
			tweakuse.setText(getString(R.string.nottweak));
		}
		arrow = prefs.getBoolean("arrow", true);
		if (arrow == false) {
			ImageView left = (ImageView) findViewById(R.id.imageView1);
			ImageView right = (ImageView) findViewById(R.id.imageView2);
			left.setVisibility(4);
			right.setVisibility(4);
		}
		mIndicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				int id = mPager.getCurrentItem();
				if (arrow == true) {
					if (id == 0) {
						right.setVisibility(4);
					} else if (id == 2) {
						left.setVisibility(4);
					} else if (id == 1) {
						left.setVisibility(0);
						right.setVisibility(0);
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			startActivity(new Intent(this, MainActivity.class));
		} else if (id == R.id.menu_setting) {
			startActivity(new Intent(Tweak.this, PreferenceActivity.class));
		} else if (id == R.id.menu_delete) {
			AlertDialog.Builder narae = new AlertDialog.Builder(Tweak.this);
			narae.setMessage(R.string.success_to_delete_);
			narae.setPositiveButton(android.R.string.yes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			narae.show();
			editor.remove("tweak");
			editor.commit();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.tweak_menu, menu);
		return true;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		finish();
	}
}