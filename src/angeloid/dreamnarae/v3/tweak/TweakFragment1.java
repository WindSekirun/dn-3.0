package angeloid.dreamnarae.v3.tweak;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import angeloid.dreamnarae.v3.NotRoot;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.main.MainActivity;

import com.stericson.RootTools.RootTools;

public class TweakFragment1 extends Fragment {

	RelativeLayout rl_angel;
	String strLanguage;
	String tweakName;
	String assetTxt;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	Locale systemLocale;
	String isPro;
	ImageView imageView1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.angel, container, false);
		rl_angel = (RelativeLayout) v.findViewById(R.id.rl_angel);
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		isPro = prefs.getString("p", "off");
		tweakName = prefs.getString("tweak", null);
		imageView1 = (ImageView) v.findViewById(R.id.imageView1);
		systemLocale = getResources().getConfiguration().locale;
		strLanguage = systemLocale.getLanguage();
		rl_angel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (MainActivity.isPro()) {
					startActivity(new Intent(getActivity(),
							Angel_Biling.class));
				if (RootTools.isAccessGiven() == true) {
					
					} else {
						AlertDialog.Builder narae = new AlertDialog.Builder(
								getActivity());
						narae.setTitle(getString(R.string.angelcheck));
						narae.setMessage(getString(R.string.angel_summary));
						narae.setPositiveButton(android.R.string.yes,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
										if (tweakName != null) {
											if (tweakName.equals("Angel")) {
												AlertDialog.Builder narae = new AlertDialog.Builder(
														getActivity());
												narae.setMessage(getString(R.string.alreadyinstall));
												narae.setPositiveButton(
														android.R.string.yes,
														new DialogInterface.OnClickListener() {
															@Override
															public void onClick(
																	DialogInterface dialog,
																	int which) {
																dialog.dismiss();
															}
														});
												narae.show();
											} else if (tweakName
													.equals("Fable")) {
												AlertDialog.Builder narae = new AlertDialog.Builder(
														getActivity());
												narae.setMessage(getString(R.string.alreadyfable));
												narae.setPositiveButton(
														android.R.string.yes,
														new DialogInterface.OnClickListener() {
															@Override
															public void onClick(
																	DialogInterface dialog,
																	int which) {
																dialog.dismiss();
																NaraeCheck("Angel");
															}
														});
												narae.setNegativeButton(
														android.R.string.no,
														new DialogInterface.OnClickListener() {

															@Override
															public void onClick(
																	DialogInterface dialog,
																	int which) {
																dialog.dismiss();

															}
														});
												narae.show();
											} else if (tweakName
													.equals("Polar")) {
												AlertDialog.Builder narae = new AlertDialog.Builder(
														getActivity());
												narae.setMessage(getString(R.string.alreadypolar));
												narae.setPositiveButton(
														android.R.string.yes,
														new DialogInterface.OnClickListener() {
															@Override
															public void onClick(
																	DialogInterface dialog,
																	int which) {
																dialog.dismiss();
																NaraeCheck("Angel");
															}
														});
												narae.setNegativeButton(
														android.R.string.no,
														new DialogInterface.OnClickListener() {

															@Override
															public void onClick(
																	DialogInterface dialog,
																	int which) {
																dialog.dismiss();

															}
														});
												narae.show();
											}
										} else {
											NaraeCheck("Angel");
										}
									}
								});
						narae.setNegativeButton(android.R.string.no,
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();

									}
								});
						narae.show();
					}
				} else {
					startActivity(new Intent(getActivity(), NotRoot.class));
				}
			}
		});
		return v;
	}

	public void NaraeCheck(final String TweakName) {
		AlertDialog.Builder narae = new AlertDialog.Builder(getActivity());
		narae.setMessage(getString(R.string.naraecheck));
		narae.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new Narae(getActivity()).execute("Angel");
					}
				});

		narae.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new Setprop(getActivity()).execute("Angel");
					}
				});

		narae.setNeutralButton(R.string.narae_,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Dialog dialog1 = new Dialog(getActivity());
						dialog1.setContentView(R.layout.license);
						dialog1.setTitle(R.string.narae_);
						dialog1.setCancelable(true);
						TextView text = (TextView) dialog1
								.findViewById(R.id.updatetext);
						try {
							if (strLanguage.equals("ko")) {
								assetTxt = readText("Binary/narae(kor).txt");
							} else {
								assetTxt = readText("Binary/narae(eng).txt");
							}
							text.setText(assetTxt);
							dialog1.show();
						} catch (IOException e) {
						}
					}
				});

		narae.show();
	}

	private String readText(String file) throws IOException {
		InputStream is = getActivity().getAssets().open(file);

		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();

		String text = new String(buffer);

		return text;
	}
}