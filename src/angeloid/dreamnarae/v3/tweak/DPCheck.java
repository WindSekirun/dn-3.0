package angeloid.dreamnarae.v3.tweak;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.ProgressBar;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import angeloid.dreamnarae.v3.NotRoot;
import angeloid.dreamnarae.v3.PreferenceActivity;
import angeloid.dreamnarae.v3.R;
import angeloid.dreamnarae.v3.dp.Angel_DP;
import angeloid.dreamnarae.v3.dp.Angel_DP_Setprop;
import angeloid.dreamnarae.v3.dp.Fable_DP;
import angeloid.dreamnarae.v3.dp.Fable_DP_Setprop;
import angeloid.dreamnarae.v3.dp.Polar_DP;
import angeloid.dreamnarae.v3.dp.Polar_DP_Setprop;
import angeloid.dreamnarae.v3.main.MainActivity;

import com.stericson.RootTools.RootTools;

public class DPCheck extends Activity {
	ArrayList<String> property;

	double Rate;

	String mDispPattern = "0.#";
	String mDispPattern1 = "0";
	String tweakName;
	String strLanguage;

	TextView percent;
	TextView devicename;
	TextView with;
	TextView with1;
	TextView TweakName;

	DecimalFormat mForm;
	DecimalFormat mForm1;

	SharedPreferences prefs;
	
	Button measure;
	
	ProgressBar pb;
	
	Locale systemLocale;
	public static DPCheck act;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		act = this;
		prefs = PreferenceManager.getDefaultSharedPreferences(DPCheck.this);
		String DP_Check = prefs.getString("rate_on", "off");
		if (DP_Check.equals("off")) {
			setContentView(R.layout.notdp);
			getSupportActionBar().setTitle(R.string.dreamnarae_percent);
			getSupportActionBar().setBackgroundDrawable(
					new ColorDrawable(0xffffffff));
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			getSupportActionBar().setIcon(R.drawable.icon);
			measure = (Button) findViewById(R.id.button1);
			measure.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (RootTools.isAccessGiven() == true) {
						calcutePercent();
					} else {
						startActivity(new Intent(DPCheck.this, NotRoot.class));
					}
				}
			});
		} else if (DP_Check.equals("on")) {
			setContentView(R.layout.dplayout);
			getSupportActionBar().setTitle(R.string.dreamnarae_percent);
			getSupportActionBar().setBackgroundDrawable(
					new ColorDrawable(0xffffffff));
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			getSupportActionBar().setIcon(R.drawable.icon);
			systemLocale = getResources().getConfiguration().locale;
			strLanguage = systemLocale.getLanguage();
			property = new ArrayList<String>();
			percent = (TextView) findViewById(R.id.percent);
			devicename = (TextView) findViewById(R.id.mydevice);
			TweakName = (TextView) findViewById(R.id.TweakName);
			pb = (ProgressBar) findViewById(R.id.pb);
			with = (TextView) findViewById(R.id.with);
			with1 = (TextView) findViewById(R.id.with1);
			mForm = new DecimalFormat(mDispPattern);
			mForm1 = new DecimalFormat(mDispPattern1);
			prefs = PreferenceManager.getDefaultSharedPreferences(DPCheck.this);
			String percent1 = prefs.getString("rate_percent", "0");
			String Tweakname = prefs.getString("rate_tweak", null);
			if (strLanguage.equals("ko")) {
				with.setVisibility(4);
			} else {
				with1.setVisibility(4);
			}
			double percent_double = Double.valueOf(percent1);
			percent.setText(mForm.format(percent_double) + "%");
			TweakName.setText(Tweakname);
			int percent_int = (int) percent_double;
			pb.setProgress(Integer.valueOf(mForm.format(percent_int)));
			devicename.setText(Build.MODEL);
		}
	}
	
	public void calcutePercent() {
		AlertDialog.Builder narae = new AlertDialog.Builder(DPCheck.this);
		narae.setMessage(getString(R.string.anydp));
		narae.setPositiveButton(R.string.Angel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (new File("/system/bin/narae").exists()) {
							new Angel_DP(DPCheck.this).execute("Angel");
						} else {
							new Angel_DP_Setprop(DPCheck.this).execute("Angel");
						}	
					}
				});

		narae.setNeutralButton(R.string.Fable,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (new File("/system/bin/narae").exists()) {
							new Fable_DP(DPCheck.this).execute("Fable");
						} else {
							new Fable_DP_Setprop(DPCheck.this).execute("Fable");
						}	
					}
				});

		narae.setNegativeButton(R.string.Polar,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (new File("/system/bin/narae").exists()) {
							new Polar_DP(DPCheck.this).execute("Polar");
						} else {
							new Polar_DP_Setprop(DPCheck.this).execute("Polar");
						}
					}
				});
		narae.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			startActivity(new Intent(this, MainActivity.class));
		} else if (id == R.id.menu_setting) {
			startActivity(new Intent(DPCheck.this,
					PreferenceActivity.class));
		} else if (id == R.id.menu_dp) {
			if (RootTools.isAccessGiven() == true) {
				calcutePercent();
			} else {
				startActivity(new Intent(DPCheck.this, NotRoot.class));
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.dp_menu, menu);
		return true;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		finish();
	}
	
}