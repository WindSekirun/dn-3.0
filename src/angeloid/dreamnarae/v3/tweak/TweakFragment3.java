package angeloid.dreamnarae.v3.tweak;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import angeloid.dreamnarae.v3.NotRoot;
import angeloid.dreamnarae.v3.R;

import com.stericson.RootTools.RootTools;

public class TweakFragment3 extends Fragment {

	Locale systemLocale;
	RelativeLayout rl_polar;
	boolean isPro;
	String strLanguage;
	String tweakName;
	String assetTxt;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.polar, container, false);
		rl_polar = (RelativeLayout) v.findViewById(R.id.rl_polar);
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		tweakName = prefs.getString("tweak", null);
		systemLocale = getResources().getConfiguration().locale;
		strLanguage = systemLocale.getLanguage();
		rl_polar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (RootTools.isAccessGiven() == true) {
					AlertDialog.Builder narae = new AlertDialog.Builder(
							getActivity());
					narae.setTitle(getString(R.string.polarcheck));
					narae.setMessage(getString(R.string.polar_summary));
					narae.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									if (tweakName != null) {
										if (tweakName.equals("Polar")) {
											AlertDialog.Builder narae = new AlertDialog.Builder(
													getActivity());
											narae.setMessage(getString(R.string.alreadyinstall));
											narae.setPositiveButton(
													android.R.string.yes,
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															dialog.dismiss();
														}
													});
											narae.show();

										} else if (tweakName.equals("Angel")) {
											AlertDialog.Builder narae = new AlertDialog.Builder(
													getActivity());
											narae.setMessage(getString(R.string.alreadyangel));
											narae.setPositiveButton(
													android.R.string.yes,
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															dialog.dismiss();
															NaraeCheck("Polar");
														}
													});
											narae.setNegativeButton(
													android.R.string.no,
													new DialogInterface.OnClickListener() {

														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															dialog.dismiss();

														}
													});
											narae.show();

										} else if (tweakName.equals("Fable")) {
											AlertDialog.Builder narae = new AlertDialog.Builder(
													getActivity());
											narae.setMessage(getString(R.string.alreadyfable));
											narae.setPositiveButton(
													android.R.string.yes,
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															dialog.dismiss();
															NaraeCheck("Polar");
														}
													});
											narae.setNegativeButton(
													android.R.string.no,
													new DialogInterface.OnClickListener() {

														@Override
														public void onClick(
																DialogInterface dialog,
																int which) {
															dialog.dismiss();

														}
													});
											narae.show();

										}
									} else {
										NaraeCheck("Polar");
									}
								}
							});

					narae.setNegativeButton(android.R.string.no,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();

								}
							});
					narae.show();
				} else {
					startActivity(new Intent(getActivity(), NotRoot.class));
				}
			}
		});
		return v;
	}

	public void NaraeCheck(final String TweakName) {
		AlertDialog.Builder narae = new AlertDialog.Builder(getActivity());
		narae.setMessage(getString(R.string.naraecheck));
		narae.setPositiveButton(android.R.string.yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new Narae(getActivity()).execute("Polar");
					}
				});

		narae.setNegativeButton(android.R.string.no,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new Setprop(getActivity()).execute("Polar");
					}
				});

		narae.setNeutralButton(R.string.narae_,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Dialog dialog1 = new Dialog(getActivity());
						dialog1.setContentView(R.layout.license);
						dialog1.setTitle(R.string.narae_);
						dialog1.setCancelable(true);
						TextView text = (TextView) dialog1
								.findViewById(R.id.updatetext);
						try {
							if (strLanguage.equals("ko")) {
								assetTxt = readText("Binary/narae(kor).txt");
							} else {
								assetTxt = readText("Binary/narae(eng).txt");
							}
							text.setText(assetTxt);
							dialog1.show();
						} catch (IOException e) {
						}
					}
				});

		narae.show();
	}

	private String readText(String file) throws IOException {
		InputStream is = getActivity().getAssets().open(file);

		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();

		String text = new String(buffer);

		return text;
	}
}