package angeloid.dreamnarae.v3;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import angeloid.dreamnarae.v3.main.MainActivity;

public class SendReport extends Activity {
	EditText mail;
	EditText subject;
	EditText body;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		setContentView(R.layout.mailsender);
		getSupportActionBar().setTitle(R.string.sendmail);
		getSupportActionBar().setIcon(R.drawable.icon);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xffffffff));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		mail = (EditText) findViewById(R.id.mail);
		subject = (EditText) findViewById(R.id.subject);
		body = (EditText) findViewById(R.id.body);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			startActivity(new Intent(this, MainActivity.class));
		} else if (id == R.id.menu_setting) {
			startActivity(new Intent(this, PreferenceActivity.class));
		} else if (id == R.id.menu_mail) {
			if (!mail.getText().toString().equals("")) {
				if (!body.getText().toString().equals("")) {
					if (!subject.getText().toString().equals("")) {
						new SendMail(SendReport.this).execute();
					} else {
						Toast.makeText(SendReport.this, "Check Form",
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(SendReport.this, "Check Form",
							Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(SendReport.this, "Check Form",
						Toast.LENGTH_SHORT).show();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mail_menu, menu);
		return true;
	}

	public class SendMail extends AsyncTask<Void, Void, Void> {
		
		private ProgressDialog mDlg;
		private Context C;
		
		public SendMail(Context context) {
			C = context;
		}

		@Override
		protected void onPreExecute() {
			mDlg = new ProgressDialog(C);
			mDlg.setMessage(C.getString(R.string.sending_mail_));
			mDlg.setIndeterminate(false);
			mDlg.setCancelable(false);
			mDlg.setCanceledOnTouchOutside(false);
			mDlg.show();
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			Toast.makeText(SendReport.this, R.string.success_send_report,
					Toast.LENGTH_SHORT).show();
			mDlg.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
									"angeloidekaros@gmail.com", "spdlxm6758");
						}
					});

			try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(mail.getText().toString()));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse("help@angeloiddev.com"));
				message.setSubject("[DreamNarae] "
						+ subject.getText().toString());
				message.setContent(
						body.getText().toString()
								+ " - "
								+ mail.getText().toString()
								+ " - ",
						"text/html; charset=utf-8");

				Transport.send(message);

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}

			return null;
		}
	}
}