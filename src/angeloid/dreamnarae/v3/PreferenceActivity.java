package angeloid.dreamnarae.v3;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.preference.CheckBoxPreference;
import org.holoeverywhere.preference.ListPreference;
import org.holoeverywhere.preference.Preference;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import angeloid.dreamnarae.v3.main.MainActivity;


public class PreferenceActivity extends 	org.holoeverywhere.preference.PreferenceActivity {

	Preference license;
	Preference sublicense;
	Preference republish;

	CheckBoxPreference notification;
	CheckBoxPreference entropy;
	CheckBoxPreference sendlogcat;
	CheckBoxPreference entropynoti;
	ListPreference entropyterm;

	Preference version;
	Preference tweakreport;
	Preference appreport;

	private String assetTxt;
	Locale systemLocale;
	String strLanguage;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle(R.string.settings);
		getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(0xffffffff));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setIcon(R.drawable.icon);
		systemLocale = getResources().getConfiguration().locale;
		strLanguage = systemLocale.getLanguage();
		addPreferencesFromResource(R.xml.settings);

		license = (Preference) findPreference("license");
		sublicense = (Preference) findPreference("sublicense");
		republish = (Preference) findPreference("republish");

		notification = (CheckBoxPreference) findPreference("notification");
		entropy = (CheckBoxPreference) findPreference("entropy");
		sendlogcat = (CheckBoxPreference) findPreference("sendlogcat");
		entropyterm = (ListPreference) findPreference("entropyterm");
		entropynoti = (CheckBoxPreference) findPreference("entropynoti");

		version = (Preference) findPreference("version");
		tweakreport = (Preference) findPreference("tweakreport");
		appreport = (Preference) findPreference("appreport");

		license.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				Dialog dialog = new Dialog(PreferenceActivity.this);
				dialog.setContentView(R.layout.license);
				dialog.setTitle(R.string.license);
				dialog.setCancelable(true);
				TextView text = (TextView) dialog.findViewById(R.id.updatetext);
				try {
					if (strLanguage.equals("ko")) {
						assetTxt = readText("License/License(kor).txt");
					} else {
						assetTxt = readText("License/License(eng).txt");
					}
					text.setText(assetTxt);
					dialog.show();
				} catch (IOException e) {
				}
				return true;
			}
		});

		sublicense
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference arg0) {
						Dialog dialog = new Dialog(PreferenceActivity.this);
						dialog.setContentView(R.layout.license);
						dialog.setTitle(R.string.third_license_notice);
						dialog.setCancelable(true);
						TextView text = (TextView) dialog
								.findViewById(R.id.updatetext);
						try {
							if (strLanguage.equals("ko")) {
								assetTxt = readText("License/SubLicense(kor).txt");
							} else {
								assetTxt = readText("License/SubLicense(eng).txt");
							}
							text.setText(assetTxt);
							dialog.show();
						} catch (IOException e) {
						}
						return true;
					}
				});

		republish
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference arg0) {
						Dialog dialog = new Dialog(PreferenceActivity.this);
						dialog.setContentView(R.layout.license);
						dialog.setTitle(R.string.dn_tweak_republish);
						dialog.setCancelable(true);
						TextView text = (TextView) dialog
								.findViewById(R.id.updatetext);
						try {
							if (strLanguage.equals("ko")) {
								assetTxt = readText("License/Republish(kor).txt");
							} else {
								assetTxt = readText("License/Republish(eng).txt");
							}
							text.setText(assetTxt);
							dialog.show();
						} catch (IOException e) {
						}
						return true;
					}
				});
		appreport
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						Intent i = new Intent(PreferenceActivity.this,
								SendReport.class);
						startActivity(i);
						return true;
					}
				});
		tweakreport
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						Intent i = new Intent(PreferenceActivity.this,
								SendReport.class);
						startActivity(i);
						return true;
					}
				});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			startActivity(new Intent(this, MainActivity.class));
		}
		return super.onOptionsItemSelected(item);
	}

	private String readText(String file) throws IOException {
		InputStream is = getAssets().open(file);

		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();

		String text = new String(buffer);

		return text;
	}
}